import { v4 as uuid4 } from 'uuid'
import { TelegrafContext } from 'telegraf/typings/context'
import { handleErrors } from '../util'
import { User, State } from '../models/user'
import { setNameStep2 } from './user'
import { sendMessage, replyStep2 } from './messaging'

export const messageHandler = async (ctx: TelegrafContext) => {
    const user = await User.findOne(
        { telegramId: String(ctx.from?.id) },
        { relations: ['blocked', 'blockedBy'] }
    )

    if (!user) {
        ctx.reply('Not allowed')
        return
    }

    if (user.uid) {
        return switcher(ctx, user)
    }

    user.uid = uuid4()
    user.save()
        .then(user => switcher(ctx, user))
        .catch(error => handleErrors(ctx, error))
}

const switcher = async (ctx: TelegrafContext, user: User) => {
    switch (user.state) {
        case State.SET_NAME: {
            await setNameStep2(ctx, user)
            break
        }
        case State.MESSAGING: {
            await sendMessage(ctx, user, false)
            break
        }
        case State.RANDOM: {
            await sendMessage(ctx, user, true)
            break
        }
        case State.REPLY: {
            await replyStep2(ctx, user)
            break
        }
    }
}
