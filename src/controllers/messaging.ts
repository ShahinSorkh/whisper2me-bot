import { Markup } from 'telegraf'
import { TelegrafContext } from 'telegraf/typings/context'
import { handleErrors } from '../util'
import { User, State, findUserById, findRandomUser } from '../models/user'

export const sendMessage = async (ctx: TelegrafContext, user: User, random: boolean) => {
    const userIsBlocked = user.blockedBy.findIndex(u => u.uid == user.messagingTo) > -1
    if (!random && userIsBlocked) {
        ctx.reply('شما نمی‌توانید به این کاربر پیام بدهید')
        return
    }

    const contact = await (random ? findRandomUser(user.blockedBy) : findUserById(user.messagingTo))
    if (!contact) {
        ctx.reply('Not allowed')
        return
    }

    generalSendMessage(ctx, user, contact, random)
        .then(() => {
            user.state = State.IDLE
            user.messagingTo = null
            return user.save()
        })
        .then(() => ctx.reply('پیام شما ارسال شد'))
        .catch(error => {
            if (error == 'typeNotSupported') {
                ctx.reply('این نوع پیام پشتیبانی نمی‌شود لطفا برای اضافه کردن آن اینجا گزارش کنید\nhttps://gitlab.com/molaeiali/whisper2me-bot')
                return
            }

            handleErrors(ctx, error)
        })
}

export const reply = async (ctx: TelegrafContext, user: User, contactId: string, messageId: number) => {
    const contact = await findUserById(contactId)

    if (!contact) {
        handleErrors(ctx, '!contact')
        return
    }

    const userIsBlocked = user.blockedBy.findIndex(u => u.id == contact.id) > -1
    if (userIsBlocked) {
        ctx.reply('شما نمی‌توانید به این کاربر پیام بدهید')
        return
    }

    user.state = State.REPLY
    user.messagingTo = contactId
    user.replyingTo = messageId
    user.save()
        .then(() => ctx.reply('درحال پاسخ به 👆: پاسخ خود را بنویسید', { reply_to_message_id: ctx.update?.callback_query?.message?.message_id }))
        .catch(error => handleErrors(ctx, error))
}

export const replyStep2 = async (ctx: TelegrafContext, user: User) => {
    const messagingTo = await findUserById(user.messagingTo)

    if (!messagingTo) {
        ctx.reply('Not allowed')
        return
    }

    generalSendMessage(ctx, user, messagingTo)
        .then(() => {
            user.state = State.IDLE
            user.replyingTo = null
            user.messagingTo = null
            return user.save()
        })
        .then(() => ctx.reply('پیام شما ارسال شد'))
        .catch(error => {
            if (error == 'typeNotSupported') {
                ctx.reply('این نوع پیام پشتیبانی نمی‌شود لطفا برای اضافه کردن آن اینجا گزارش کkید\nhttps://gitlab.com/molaeiali/whisper2me-bot')
                return
            }

            if (error?.code == 400 && error?.description == 'Bad Request: reply message not found') {
                user.replyingTo = null
                replyStep2(ctx, user)
                return
            }

            handleErrors(ctx, error)
        })
}

const generalSendMessage = async (
    ctx: TelegrafContext,
    { replyingTo, uid: userId }: User,
    { telegramId: chatId, uid: contactId }: User,
    random: boolean = false
) => {
    const selfMessage = userId == contactId
    const messageHeader = random ? 'پیام تصادفی جدید' : 'پیام جدید'
    const prependMessageHeader = (text: string) => `${selfMessage ? '' : messageHeader + ': '}${text}`

    const inlineKeyboard = Markup.inlineKeyboard([
        [
            Markup.callbackButton('Block', `block-${userId}`),
            Markup.callbackButton('Reply', `reply-${userId}-${ctx.message?.message_id}`)
        ]
    ])

    const extra = {
        reply_to_message_id: replyingTo || undefined,
        reply_markup: inlineKeyboard
    }

    const extraWithCaption = {
        caption: prependMessageHeader(!selfMessage && ctx.message?.caption ? `: ${ctx.message.caption}` : ''),
        ...extra
    }

    if (ctx.message?.document) {
        return ctx.telegram.sendDocument(chatId, ctx.message?.document.file_id, extraWithCaption)
    }

    if (ctx.message?.video) {
        return ctx.telegram.sendVideo(chatId, ctx.message?.video.file_id, extraWithCaption)
    }

    if (ctx.message?.photo) {
        return ctx.telegram.sendPhoto(chatId, ctx.message?.photo[0].file_id, extraWithCaption)
    }

    if (ctx.message?.voice) {
        return ctx.telegram.sendVoice(chatId, ctx.message?.voice.file_id, extraWithCaption)
    }

    if (ctx.message?.sticker) {
        try {
            await ctx.telegram.sendMessage(chatId, messageHeader)
            return await ctx.telegram.sendSticker(chatId, ctx.message?.sticker!.file_id!, extra)
        } catch (message) {
            return console.error(message)
        }
    }

    if (ctx.message?.audio) {
        return ctx.telegram.sendAudio(chatId, ctx.message?.audio.file_id, extraWithCaption)
    }

    if (ctx.message?.animation) {
        return ctx.telegram.sendAnimation(chatId, ctx.message?.animation.file_id, extraWithCaption)
    }

    if (ctx.message?.text) {
        return ctx.telegram.sendMessage(chatId, prependMessageHeader(`: ${ctx.message.text}`), extra)
    }

    throw new Error('typeNotSupported')
}
