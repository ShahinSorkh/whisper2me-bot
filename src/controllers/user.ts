import { TelegrafContext } from 'telegraf/typings/context'
import { handleErrors } from '../util'
import { User, State, findUserById } from '../models/user'

export const setName = async (ctx: TelegrafContext) => {
    let user = await findUserById(ctx.from?.id)
    if (!user) {
        ctx.reply('Not allowed')
        return
    }

    user.state = State.SET_NAME
    user.save()
        .then(() => ctx.reply('نام مورد نظر را وارد کنید'))
        .catch(error => handleErrors(ctx, error))
}

export const randomMessage = async (ctx: TelegrafContext) => {
    let user = await findUserById(ctx.from?.id)
    if (!user) {
        ctx.reply('Not allowed')
        return
    }

    user.state = State.RANDOM
    user.save()
        .then(() => ctx.reply('پیامت رو بنویس!'))
        .catch(error => handleErrors(ctx, error))
}

export const setNameStep2 = async (ctx: TelegrafContext, user: User) => {
    user.name = ctx.message!.text || 'ناشناس'
    user.state = State.IDLE
    user.save()
        .then(user => ctx.reply(`نام شما ثبت شد: ${user.name}`))
        .catch(error => handleErrors(ctx, error))
}

export const block = async (ctx: TelegrafContext, user: User, toBlock: string) => {
    const contact = await findUserById(toBlock, true)
    if (!contact) {
        ctx.reply('Not allowed')
        return
    }

    user.blocked.push(contact)
    user.save()
        .then(() => ctx.reply('کاربر مورد نظر بلاک شد و دیگر نمی‌تواند به شما پیام دهد'))
        .catch(error => handleErrors(ctx, error))
}

export const unblock = async (ctx: TelegrafContext) => {
    const user = await findUserById(ctx.from?.id, true)
    if (!user) {
        ctx.reply('Not allowed')
        return
    }

    if (user.blocked.length) {
        user.blocked = []
        return user.save()
            .then(() => ctx.reply('همه کاربرانی که بلاک کرده بودید رفع بلاک شدند و می‌توانند به شما پیام دهند'))
    }

    ctx.reply('شما هیچکس را بلاک نکرده‌اید')
}

export const mylink = async (ctx: TelegrafContext) => {
    const user = await findUserById(ctx.from?.id, true)
    if (!user) {
        ctx.reply('Not allowed')
        return
    }

    ctx.reply(`https://t.me/whisper2me_bot?start=${user.id}`)
}
