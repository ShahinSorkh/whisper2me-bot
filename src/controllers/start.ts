import { v4 as uuid4 } from 'uuid'
import { TC } from '../telegraf'
import { handleErrors } from '../util'
import { User, State, findUserById } from '../models/user'

const greeting =
    'به Whisper2Me خوش اومدی\n' +
    'برای گرفتن لینک خودت /my_link رو بفرست\n' +
    'برای تغییر نام خودت /set_name رو بفرست (ناشناس به صورت پیشفرض)\n' +
    'برای  رفع بلاک همه کاربران /unblock رو بفرست\n' +
    'برای دیدن سوالات متداول /faq رو بفرست\n'

export const start = async (ctx: TC): Promise<void> => {
    let user = await findUserById(ctx.from?.id)
    if (!user) {
        user = new User()
        user.telegramId = String(ctx.from?.id)
        user.uid = uuid4()
        return user.save()
            .then(user => payload(ctx, user))
            .catch(error => handleErrors(ctx, error))
    }

    payload(ctx, user)
}

export const payload = async (ctx: TC, user: User) => {
    if (!user) {
        ctx.reply('Not allowed')
        return
    }

    const contact = await findUserById(ctx.startPayload)
    if (!contact) {
        ctx.reply(greeting)
        return
    }

    user.state = State.MESSAGING
    user.messagingTo = contact.uid ? contact.uid : String(contact.id)
    user.replyingTo = null
    user.save()
        .then(() => ctx.reply(`${greeting} \n\n شما از طریق لینک '${contact!.name}' وارد شده‌اید، پیامت به '${contact!.name}' رو بنویس`))
        .catch(error => handleErrors(ctx, error))
}
