import { TelegrafContext } from 'telegraf/typings/context'
import { findUserById } from '../models/user'
import { reply } from './messaging'
import { block } from './user'

export const callbackHandler = async (ctx: TelegrafContext) => {
    const user = await findUserById(ctx.from?.id)
    if (!user) {
        ctx.reply('Not allowed')
        return
    }

    const data = ctx.update.callback_query!.data
    const dataType = data?.substr(0, 6)

    if (!data || dataType !in ['reply-', 'block-']) {
        return
    }

    if (dataType == 'block-') {
        const toBlock = data.substr(6)
        return block(ctx, user, toBlock)
    }

    if (data.length > 42) {
        // New method to use uuid
        const contactId = data.substr(6, 36)
        const messageId = Number(data.substr(43))

        return reply(ctx, user, contactId, messageId)
    }

    // TODO:  Old deprecated id based method
    const [contactId, messageId] = data.substr(6).split('-')
    reply(ctx, user, contactId, Number(messageId))
}
