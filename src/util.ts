import { TelegrafContext } from 'telegraf/typings/context'

export const handleErrors = (ctx: TelegrafContext, error: any) => {
    if (error?.code == 403 && error?.description == 'Forbidden: bot was blocked by the user') {
        ctx.reply('مخاطب شما ربات را بلاک کرده است، امکان پیام به او وجود ندارد')
        return
    }

    console.error(error)
    ctx.reply('خطایی رخ داده است')
}
