import * as dotenv from 'dotenv'
import { Telegraf } from 'telegraf'
import { createConnection } from 'typeorm'
import { start } from './controllers/start'
import { messageHandler } from './controllers/message_handler'
import { setName, unblock, mylink, randomMessage } from './controllers/user'
import { callbackHandler } from './controllers/callback_handler'
import { faq } from './controllers/general'

const dotenvResult = dotenv.config()
if (dotenvResult.error) {
    throw dotenvResult.error
}

const initDb = (): Promise<void> => createConnection()
    .then(() => console.log('Connected successfully to the Database'))
    .catch(console.error)


const initBot = (): void => {
    const bot = new Telegraf(process.env.BOT_TOKEN || '')

    bot.start(start)
    bot.telegram.setMyCommands(
        [
            { command: 'my_link', description: 'برای گرفتن لینک خودت' },
            { command: 'set_name', description: 'برای تغییر نام خودت (ناشناس به صورت پیشفرض)' },
            { command: 'random', description: 'پیام تصادفی به یک کاربر دیگر' },
            { command: 'unblock', description: 'برای رفع بلاک همه کاربران' },
            { command: 'faq', description: 'سوالات متداول' },
        ]
    )

    bot.command('/my_link', mylink)
    bot.command('/set_name', setName)
    bot.command('/random', randomMessage)
    bot.command('/unblock', unblock)
    bot.command('/faq', faq)

    bot.on('callback_query', callbackHandler)
    bot.on('message', messageHandler)

    bot.launch()
        .then(() => console.log('Bot started!'))
        .catch(console.error)
}

initDb().then(initBot)
