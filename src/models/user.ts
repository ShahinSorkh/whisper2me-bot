import { Entity, BaseEntity, Column, ManyToMany, JoinTable, PrimaryGeneratedColumn, Not, In } from 'typeorm'

export enum State {
    IDLE = 'idle',
    MESSAGING = 'messaging',
    SET_NAME = 'set-name',
    REPLY = 'reply',
    UNBLOCKING = 'unblocking',
    RANDOM = 'random',
}

@Entity()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column({ unique: true, name: 'telegram_id' })
    telegramId: string

    @Column('text', { default: null })
    uid: string | null

    @Column({ default: 'ناشناس' })
    name: string

    @Column('text', { default: State.IDLE })
    state: State

    //chatId
    @Column('int', { nullable: true })
    messagingTo: string | null

    //messageId
    @Column('int', { nullable: true })
    replyingTo: number | null

    @ManyToMany(type => User, user => user.blocked)
    @JoinTable()
    blockedBy: User[]

    @ManyToMany(type => User, user => user.blockedBy)
    blocked: User[]
}

export const findUserById = async (
    id?: number|string|null,
    withBlocks: boolean = false
): Promise<User|undefined> => {
    if (!id) {
        return
    }

    const relations = withBlocks ? ['blocked', 'blockedBy'] : []
    if (typeof id === 'number' || Number(id)) {
        return await User.findOne({ telegramId: String(id) }, { relations })
    }

    return await User.findOne({ uid: id }, { relations })
}

export const findRandomUser = async (blockedUsers: User[]) => {
    const possibleUsers = await User.find({ uid: Not(In(blockedUsers.map(u => u.uid))) })
    return possibleUsers[Math.random() * possibleUsers.length | 0]
}
